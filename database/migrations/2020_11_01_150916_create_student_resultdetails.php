<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentResultdetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_resultdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('faculty_id')->nullable();
            $table->foreign('faculty_id')->references('id')->on('faculties')->onDelete('cascade');
            $table->unsignedInteger('level_id')->nullable();
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');  
            $table->unsignedInteger('course_id')->nullable();
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');            
            $table->string('semester')->nullable();
            $table->string('year')->nullable();
            $table->string('name');
            $table->string('regd_no')->nullable();
            $table->string('symbol_no')->nullable();
            $table->string('dob')->nullable();
            $table->string('institute')->nullable();
            $table->string('exam_type')->nullable();
            $table->string('sgpa')->nullable();
            $table->string('average_grade')->nullable();
            $table->string('result')->nullable();
            $table->string('remarks')->nullable();
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_resultdetails');
    }
}
