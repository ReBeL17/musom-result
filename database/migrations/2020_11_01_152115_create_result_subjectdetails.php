<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResultSubjectdetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_subjectdetals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('result_id')->nullable();
            $table->foreign('result_id')->references('id')->on('student_resultdetails')->onDelete('cascade');            
            $table->string('subject_code')->nullable();
            $table->string('subject_title')->nullable();
            $table->string('credit_hrs')->nullable();
            $table->string('grade_point')->nullable();
            $table->string('grade')->nullable();
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_subjectdetals');
    }
}
