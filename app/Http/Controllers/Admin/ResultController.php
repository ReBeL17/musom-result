<?php

namespace App\Http\Controllers\Admin;

use App\Result;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Permission;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\StoreResultRequest;
use App\Http\Requests\UpdateResultRequest;
use App\Http\Requests\MassDestroyResultRequest;
use Session;
use Auth;
use App\Course;
use App\Faculty;
use App\Sub;
use App\Imports\ResultImport;
use Maatwebsite\Excel\Facades\Excel;

class ResultController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth:admin')->except(['index','getResult']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // abort_if(Gate::denies('sub-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $faculties = Faculty::all()->pluck('name','id');

        return view('result-search', compact('faculties'));
    }

    public function courseResults($id)
    {
        abort_if(Gate::denies('result-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $results = Result::where('course_id',$id)->with('course')->get();

        return view('admin.backend.results.index', compact('results','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // dd($id);
        abort_if(Gate::denies('sub-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $course = Course::where('id',$id)->pluck('name','id');
        // dd($course);
        return view('admin.backend.subs.create', compact('course','id'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubRequest $request)
    {
        // dd($request->all());
        $data=[
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'subject_code' => $request->subject_code,
            'description' => $request->description,
            'course_id' => $request->course_id,
            'semester' => $request->semester
        ];
        $subject = Sub::create($data);

        Session::flash('flash_success', 'Subject created successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.subs.courseSubjects', $request->course_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Sub $sub)
    {
        //
        abort_if(Gate::denies('sub-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.backend.subjects.show', compact('subject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Sub $sub)
    {
        abort_if(Gate::denies('sub-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $course = Course::where('id',$sub->course_id)->pluck('name','id');
        $sub->load('course');
        return view('admin.backend.subs.edit', compact('sub','course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubRequest $request, Sub $sub)
    {
        // dd($request->all());
         $data=[
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'subject_code' => $request->subject_code,
            'description' => $request->description,
            'course_id' => $request->course_id,
            'semester' => $request->semester
        ];
        // dd($data);
        $sub->update($data);

        Session::flash('flash_success', 'Subject updated successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.subs.courseSubjects', $request->course_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        //
        abort_if(Gate::denies('result-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $result->delete();

        Session::flash('flash_danger', 'Result has been deleted !.');
        Session::flash('flash_type', 'alert-danger');
        return back();
    }

    public function massDestroy(MassDestroyResultRequest $request)
    {
        Result::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
   
    public function importResult(Request $request) 
    {
        $this->validate($request,[
            'file' => 'required|mimes:xls,xlsx|max:5000',
        ]);

        if($request->file('file')) {            
            Excel::import(new ResultImport($request->id), request()->file('file'));
            return back()->with('message','Results has been imported');
        }
           
    }

    public function getResult(Request $request) {
        return Result::with(['marks','course'])->where([
                                                    ['course_id',$request->programs],
                                                    ['semester',$request->semester],
                                                    ['symbol_no',$request->symbol],
                                                    ['year',$request->year],
                                                    ['dob',$request->dob]
                                                ])->first();
        // dd($result);
    }
}
