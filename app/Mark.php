<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Mark extends Model
{
    //
    // use SoftDeletes;

    protected $table = "result_subjectdetals";
    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'result_id',
      'subject_code',
      'subject_title',
      'credit_hrs',      
      'grade_point',
      'grade',
      'remarks',
  ];
     
     public function result() {
     
        return $this->belongsTo(Result::class);
            
     }


    //  public function getRouteKeyName() {
       
    //       return 'slug';
    //   }
}
