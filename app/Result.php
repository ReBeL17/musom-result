<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Result extends Model
{
    //
    use SoftDeletes;

    protected $table = "student_resultdetails";
    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'faculty_id', 'level_id', 'course_id', 'semester', 'year', 'name', 'regd_no', 'symbol_no', 'dob', 'institute', 'exam_type', 'average_grade', 'sgpa', 'result', 'remarks', 'created_by', 'updated_by', 'deleted_by', 'created_at', 'updated_at', 'deleted_at',
     ];
     
     public function course() {
     
        return $this->belongsTo(Course::class);
            
     }

     public function marks() {

      return $this->hasMany(Mark::class);
          
    }

    //  public function getRouteKeyName() {
       
    //       return 'slug';
    //   }
}
