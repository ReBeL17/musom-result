<?php

namespace App\Imports;

use App\Result;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Auth; 

class ResultImport implements ToModel, WithStartRow
{
    public function __construct($request_id) {
        // dd($request_id);
        $this->courseID = $request_id;
    }
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd(Date::excelToDateTimeObject($row[7])->format('Y-m-d'));
        if (isset($row[6])) {
        $result = Result::Create([
            'course_id' => $this->courseID,
            'semester' => $row[1],
            'year' => $row[2],
            'institute' => $row[3],
            'regd_no' => $row[4],
            'symbol_no' => $row[5],
            'name' => $row[6],
            'dob' => Date::excelToDateTimeObject($row[7])->format('Y-m-d'),
            // 'average_grade' => $row[8],
            'sgpa' => $row[8],
            'result' => $row[9],
            'created_by' => Auth::user()->name,
        ]);
        }
       
        // $max = count($row);
        // $i = 10;
        // for($i;$i<=$max;$i+5){
        //     if (isset($row[$i])) {
        //         $result->marks()->create([
        //             'subject_code'=> $row[$i],
        //             'subject_title' => $row[$i+1],
        //             'grade_point'=> $row[$i+2],
        //             'grade'=> $row[$i+3],
        //             'remarks'=> $row[$i+4],
        //         ]);
        //     }
        // }

        if (isset($row[10])) {
            $result->marks()->create([
                'subject_code'=> $row[10],
                'subject_title' => $row[11],
                'grade_point'=> $row[12],
                'grade'=> $row[13],
                'remarks'=> $row[14],
            ]);
        }
        if (isset($row[15])) {
            $result->marks()->create([
                'subject_code'=> $row[15],
                'subject_title' => $row[16],
                'grade_point'=> $row[17],
                'grade'=> $row[18],
                'remarks'=> $row[19],
            ]);
        }
        if (isset($row[20])) {
            $result->marks()->create([
                'subject_code'=> $row[20],
                'subject_title' => $row[21],
                'grade_point'=> $row[22],
                'grade'=> $row[23],
                'remarks'=> $row[24],
            ]);
        }
        if (isset($row[25])) {
            $result->marks()->create([
                'subject_code'=> $row[25],
                'subject_title' => $row[26],
                'grade_point'=> $row[27],
                'grade'=> $row[28],
                'remarks'=> $row[29],
            ]);
        }
        if (isset($row[30])) {
            $result->marks()->create([
                'subject_code'=> $row[30],
                'subject_title' => $row[31],
                'grade_point'=> $row[32],
                'grade'=> $row[33],
                'remarks'=> $row[34],
            ]);
        }
        if (isset($row[35])) {
            $result->marks()->create([
                'subject_code'=> $row[35],
                'subject_title' => $row[36],
                'grade_point'=> $row[37],
                'grade'=> $row[38],
                'remarks'=> $row[39],
            ]);
        }
        return $result;         
              
    }

    /**
    * @return int
    */
    public function startRow(): int
    {
        return 2;
    }
}
